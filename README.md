MOSW_test game for Minetest
==========================================================

TODO:
* nyan cats
* adjust ratios and ends of stone generator
    * add gravel seams between certain layers
    * DONE - reduce and multiply coal layer
    * limestone
    * salt
    * feldspar
    * pumice
    * pyrite
    * DONE - anthracite coal
    * DONE - jade
    * conglomerate
* biomes. lots of biomes.
    * swamp
    * peat bog
    * tiaga plains
    * boreal forest
    * rainforest
    * (southern) pine forest
    * temperate grass plains
    * savanna plains
    * various farm-plant-containing mini-biomes
    * DONE - barren sandy desert
    * arizona desert
    * sage scrubs
    * icebergs
    * shallow ice sheets, with random holes if possible?
    * kelp forest, if possible
    * bamboo forest, if someone gets around to making bamboo
    * DONE - barren gravelly mountain tops
    * DONE - sandy beaches
    * DONE - gravelly beaches
    * DONE - deep ocean floor
    * oak forests
    * tropical mangroves (with rubber and willow trees, papyrus)
    * grasslands dusted with surface snow
    * active volcano, with lava (might kill performance with forest fires...)
    * salt flats
* coal-powered technic machines
* DONE - adjust crafts for stone tools with more rocks
* make fire spread much slower - implemented but not tested
* pretty lights
* DONE - large torches (might consider making custom textured ones for each stone)
* mesecons
* farming
* farming mapgen
* make moretrees play nice
* more ores?
* license and credits
* bridges
* nyan tools
* pipeworks
* wrench from technic
* glowing mushrooms in caves (need texture and node box)
* floor-placing/wall building mod
* stairsplus, plus integration with a million rock types


nodes needed
=====================================
* burried peat
* top peat
* salt
* limestone
* jade
* pyrite
* conglomerate
* anthracite
* feldspar
* pumice
* various sage brush
* glowing mushrooms
* torch base
* animated torch fire
* permafrost
* savanna grasses of some kind?


=====================================

To use this game with Minetest, insert this repository as
  /games/MOSW_test
in the Minetest Engine.

The Minetest Engine can be found in:
  https://github.com/minetest/minetest/

Compatibility
--------------
The MOSW_test github master HEAD is generally compatible with the github
master HEAD of minetest.

Additionally, when the minetest engine is tagged to be a certain version (eg.
0.4.10), MOSW_test is tagged with the version too.

When stable releases are made, MOSW_test is packaged and made available in
  http://minetest.net/download.php
and in case the repository has grown too much, it may be reset. In that sense,
this is not a "real" git repository. (Package maintainers please note!)



License of MOSW_test source code
----------------------
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org>





License of minetest_game source code
----------------------
Copyright (C) 2010-2012 celeron55, Perttu Ahola <celeron55@gmail.com>
See README.txt in each mod directory for information about other authors.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

License of media (textures and sounds)
--------------------------------------
Copyright (C) 2010-2012 celeron55, Perttu Ahola <celeron55@gmail.com>
See README.txt in each mod directory for information about other authors.

Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)
http://creativecommons.org/licenses/by-sa/3.0/

License of menu/header.png
Copyright (C) 2013 BlockMen CC BY-3.0