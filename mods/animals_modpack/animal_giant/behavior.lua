
-- only child of the root node


function tprint (tbl, indent)
	local formatting = ""
  if not indent then indent = 0 end
  if tbl == nil then 
	  print(formatting .. "nil") 
	return
  end
  for k, v in pairs(tbl) do
    formatting = string.rep("  ", indent) .. k .. ": "
    if type(v) == "table" then
      print(formatting)
      tprint(v, indent+1)
    elseif type(v) == 'boolean' then
      print(formatting .. tostring(v))      
    elseif type(v) == 'function' then
      print(formatting .. "[function]")      
    elseif type(v) == 'userdata' then
      print(formatting .. "[userdata]")      
    else
      print(formatting .. v)
    end
  end
end


function mkSelector(name, list)
	
	return {
		name=name,
		kind="selector",
		current_kid=-1,
		kids=list,
		
		reset=selector_reset,
		tick=selector_tick,
	}
end


function mkSequence(name, list)
	
	return {
		name=name,
		kind="sequence",
		current_kid=-1,
		kids=list,
		
		reset=sequence_reset,
		tick=sequence_tick,
	}
	
	
end

function mkRepeat(name, what)
	
	return {
		name=name,
		kind="repeat",
		kids = what,
		
		reset=repeat_reset,
		tick=repeat_tick,
	}
	
	
end


function selector_reset(node, data)
	node.current_kid = -1
end
function sequence_reset(node, data)
	node.current_kid = -1
end

function repeat_reset(node, data)
	
end

function repeat_tick(node, data)
	--tprint(node)
	local ret = node.kids[1]:tick(data)
	if ret ~= "running" then
		print("repeat resetting")

		node.kids[1]:reset(data)
	end

		print("repeat ending\n&&&&&")

	return "success"
end


-- nodes never call :reset() on themselves


function selector_tick(node, data) 
	
	local ret
	
	if node.current_kid  == -1 then
		node.current_kid = 1
		node.kids[node.current_kid ]:reset(data)
	end
	
	repeat
		ret = node.kids[node.current_kid ]:tick(data)
		if ret == "running" or ret == "success" then
			return ret
		end
		
		-- ret == failed
		node.current_kid = node.current_kid  + 1
		node.kids[node.current_kid ]:reset(data)
		
	until node.current_kid > table.getn(node.kids)
	
	return "failed"
end




function sequence_tick(node, data) 
	
	local ret
	
	if node.current_kid  == -1 then
		node.current_kid = 1
		node.kids[node.current_kid ]:reset(data)
	end
	
	repeat
		print("+++ ticking kid "..node.current_kid)
		ret = node.kids[node.current_kid ]:tick(data)
		print(" sequence got status ["..ret.."] from kid "..node.current_kid)
		if ret == "running" or ret == "failed" then
			print(" sequence returning "..ret.." \n\n")
			return ret
		else
			print("reset kid ".. node.current_kid .. " ("..node.kids[node.current_kid].name..")" )
		
			node.current_kid = node.current_kid  + 1
			-- ret == success
			if node.kids[node.current_kid ] ~= nil then
				node.kids[node.current_kid ]:reset(data)
			end
		end
			


		
		print("\n===\ntlen: ".. table.getn(node.kids) .. " - " ..node.current_kid )
		
	until node.current_kid  > table.getn(node.kids)
	
	return "success"
end




function distance(a,b) 
	local x = a.x - b.x
	local z = a.z - b.z
	
	return math.abs(math.sqrt(x*x + z*z))
end


function mkFindNodeNear(sel, dist)
	
	return {
		name="find node near",
		kind="action",
		
		reset= function(node, data) 
			-- tprint(data)
			local targetpos = minetest.find_node_near(data.pos, dist, sel)
			tprint(targetpos)
			data.targetPos = targetpos
		end,
			
		tick= function(node, data)
			if data.targetPos == nil then 
				print("could not find node near")
				return "failed" 
			end
			
			return "success"
		end,
	}

end



function mkApproach(dist)
	
	return {
		name="go to",
		kind="action",
		
		reset= function(node, data) 
			
			if data.targetPos ~= nil then
				print("Approaching target ("..data.targetPos.x..","..data.targetPos.y..","..data.targetPos.z..")")
				p_mov_gen.set_target(data.entity, data.targetPos, false)
			else 
				print("Approach: targetPos is nil")
			end
		end,
			
		tick= function(node, data)
			if data.targetPos == nil then 
				return "failed" 
			end
			p_mov_gen.set_target(data.entity, data.targetPos, false)
			local d = distance(data.pos, data.targetPos)
			
			print("dist: "..d)
			
			if d <= dist then
				print("arrived at target")
				return "success"
			end
			
			return "running"
		end,
	}

end


function mkDestroy()
	
	return {
		name="destroy",
		kind="action",
		
		reset= function(node, data) 
			
		end,
			
		tick= function(node, data)
			print("Destroying target")
			if data.targetPos == nil then 
				return "failed" 
			end
			
			minetest.set_node(data.targetPos, {name="air"})
			
			return "success"
		end,
	}

end

















