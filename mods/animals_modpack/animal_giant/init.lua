local version = "0.1.2"

minetest.log("action","MOD: loading animal_giant ... ")

local giant_groups = {
						not_in_creative_inventory=1
					}

local selectionbox_giant = {-0.6, -2.0, -0.6, 0.6, 1.4, 0.6}

local modpath = minetest.get_modpath("animal_giant")

dofile (modpath .. "/flame.lua")
dofile (modpath .. "/behavior.lua")

local crush_time_group = {
	cobble=3,
	wood=2,
	dirt=1.5,
	sand=1,
	leaves=0.5,
}
	


function giant_drop()
	local result = {}
	if math.random() < 0.05 then
		table.insert(result,"animalmaterials:bone 4")
	else
		table.insert(result,"animalmaterials:bone 2")
	end

	table.insert(result,"animalmaterials:meat_undead 5")

	return result
end

function get_players_in_radius(pos, r)

	local all_objects = minetest.get_objects_inside_radius(pos, r)
	local players = {}
	local _,obj
	for _,obj in ipairs(all_objects) do
		if obj:is_player() then
			table.insert(players, obj)
		end
	end
	
	return players
end

function find_player_in_radius(pos, r)

	local all_objects = minetest.get_objects_inside_radius(pos, r)
	local _,obj
	for _,obj in ipairs(all_objects) do
		if obj:is_player() then
			return obj
		end
	end
	
	return nil
end

function dist(a,b) 
	local x = a.x - b.x
	local z = a.z - b.z
	
	return math.abs(math.sqrt(x*x + z*z))
end

function giant_on_step_handler(entity,now,dtime)
	local pos = entity.getbasepos(entity)
	local current_light = minetest.get_node_light(pos)
	
local new_state = mob_state.get_state_by_name(entity,"walking")
mob_state.change_state(entity,new_state)
  	if entity.dynamic_data.p_movement == nil then
  		p_mov_gen.init_dynamic_data(entity,now,{})
  	end
 		
	if entity.lastpos == nil then
		entity.lastpos = pos
	end

	if entity.btData == nil then
		entity.btData = { 
			entity= entity,
	--		lastpos= pos,
		}
	end

	print("<<< starting >>>")
	entity.btData.pos = pos
	-- entity.btData.entity = entity
	
	entity.bt:tick(entity.btData)
	
	--entity.btData.lastpos = entity.pos
	print("<<< ending >>>")

	return 
	
	
	--[[
	-- print("state: " .. entity.dynamic_data.state.current.name .. "\n")
	
	local torchpos = minetest.find_node_near(pos, 2, {"default:torch"})

	if torchpos ~= nil then
		minetest.set_node(torchpos, {name="air"})
	end
	
	
	torchpos = minetest.find_node_near(pos, 20, {"default:torch"})
	
	if torchpos ~= nil then
		p_mov_gen.set_target(entity, torchpos, false)
		return
	end
	
	
	
	local player = find_player_in_radius(pos, 30)
	if player ~= nil then
		local playerpos = player:getpos()
		p_mov_gen.set_target(entity, playerpos, false)
		
		if entity.last_d == nil then entity.last_d = 99999999 end
	
		local d = dist(playerpos, pos)
		if math.abs(d - entity.last_d) < 0.01 then
			print( "not moved much " .. math.abs(d - entity.last_d) )
			
			local np = minetest.find_node_near(pos, 2, "default:cobble")
			
			if np ~= nil then
				minetest.set_node(np, {name="air"})
			end
			
		end
		entity.last_d = d
	end
	
	]]
	--print("giant on step: current_light:" .. current_light .. " max light: "
	--	.. LIGHT_MAX .. " 3dmode:" .. dump(minetest.world_setting_get("disable_animals_3d_mode")))

-- 	if current_light ~= nil and
-- 		current_light > LIGHT_MAX and
-- 		minetest.world_setting_get("mobf_disable_3d_mode") ~= true and
-- 		minetest.world_setting_get("giant_3d_burn_animation_enabled") == true then
-- 
-- 
-- 		local xdelta = (math.random()-0.5)
-- 		local zdelta = (math.random()-0.5)
-- 		--print("receiving sun damage: " .. xdelta .. " " .. zdelta)
-- 		local newobject=minetest.add_entity( {	x=pos.x + xdelta,
-- 													y=pos.y,
-- 													z=pos.z + zdelta },
-- 										"animal_giant:giant_flame")
-- 
-- 		--add particles
-- 	end
-- 	if entity.dynamic_data.spawning.spawner == "at_night" or
-- 		entity.dynamic_data.spawning.spawner == "at_night_mapgen" then
-- 		local current_time = minetest.get_timeofday()
-- 		if (current_time > 0.15) and
-- 			(current_time < 0.30) then
-- 			if entity.last_time ~= nil then
-- 				local last_step_size = dtime /  86400 -- (24*3600)
-- 				local time_step = current_time - entity.last_time
-- 				if time_step > last_step_size * 1000 then
-- 					print("Giant: time jump detected removing mob: " .. time_step .. " last_step_size: " .. (last_step_size * 1000))
-- 					spawning.remove(entity)
-- 					--return false to abort procession of other hooks
-- 					return false
-- 				end
-- 			end
-- 		end
-- 
-- 		entity.last_time = current_time
-- 	end
end

function giant_on_activate_handler(entity)

	print("\n-------------------------------\n")
	entity.bt = mkRepeat("root", {
		mkSequence("snuff torches", {
			mkFindNodeNear({"default:torch"}, 20),
			mkApproach(2),
			mkDestroy(),
			
		})
	})
-- 	local pos = entity.object:getpos()
-- 
-- 	local current_light = minetest.get_node_light(pos)
-- 
-- 	if current_light == nil then
-- 		minetest.log(LOGLEVEL_ERROR,
-- 			"ANIMALS:Giant Bug!!! didn't get a light value for ".. printpos(pos))
-- 		return
-- 	end
	--check if animal is in sunlight
-- 	if ( current_light > LIGHT_MAX) then
-- 		--don't spawn giant in sunlight
-- 		spawning.remove(entity)
-- 	end
end

local giant_prototype = {
		name="giant",
		modname="animal_giant",

		factions = {
			member = {
				"monsters",
				"undead"
				}
			},

		generic = {
					description="Giant",
					base_health=20,
					kill_result=giant_drop,
					armor_groups= {
						fleshy=95,
						daemon=30,
					},
					groups = giant_groups,
					envid="on_ground_1",
					custom_on_step_handler = giant_on_step_handler,
					custom_on_activate_handler = giant_on_activate_handler,
					population_density=1000000,
					stepheight = 2.1,
				},
		movement =  {
					min_accel=0.1,
					max_accel=0.25,
					max_speed=1,
					pattern="stop_and_go",
					canfly=false,
					follow_speedup=5,
					},
		combat = {
					angryness=1,
					starts_attack=true,
					sun_sensitive=false,
					melee = {
						maxdamage=6,
						range=3,
						speed=1,
						},
					distance 		= nil,
					self_destruct 	= nil,
					},
		sound = {
					random = {
								name="animal_giant_random_1",
								min_delta = 10,
								chance = 0.5,
								gain = 0.05,
								max_hear_distance = 5,
								},
	
					},
		animation = {
				stand = {
					start_frame = 0,
					end_frame   = 80,
					},
				walk = {
					start_frame = 168,
					end_frame   = 188,
					},
				attack = {
					start_frame = 81,
					end_frame   = 110,
					},
			},
			states = {
				{
					name = "default",
-- 					movgen = "none",
					movgen="mgen_path",
					typical_state_time = 30,
					chance = 0,
					animation = "stand",
					graphics = {
						sprite_scale={x=4,y=4},
						sprite_div = {x=6,y=2},
						visible_height = 2.2,
						visible_width = 1,
					},
					graphics_3d = {
						visual = "mesh",
						mesh = "animal_giant_giant.b3d",
						textures = {"animal_giant_giant_mesh.png"},
						collisionbox = selectionbox_giant,
						visual_size= {x=2,y=2,z=2},
						},
				},
				{
					name = "walking",
					movgen = "mgen_path",
					pattern="stop_and_go",
-- 					movgen = "none",
					typical_state_time = 9999999,
					chance = 0.5,
					animation = "walk",
				},
-- 				{
-- 					name = "combat",
-- 					typical_state_time = 9999,
-- 					chance = 0.0,
-- 					animation = "attack",
-- 					movgen="mgen_path",
-- 				},
			}
		}


		--[[
--compatibility code
minetest.register_entity("animal_giant:giant_spawner",
 {
	physical        = false,
	collisionbox    = { 0.0,0.0,0.0,0.0,0.0,0.0},
	visual          = "sprite",
	textures        = { "invisible.png^[makealpha:128,0,0^[makealpha:128,128,0" },
	on_activate = function(self,staticdata)

		local pos = self.object:getpos();
		minetest.add_entity(pos,"mobf:compat_spawner")
		self.object:remove()
	end,
})

minetest.register_entity("animal_giant:giant_spawner_at_night",
 {
	physical        = false,
	collisionbox    = { 0.0,0.0,0.0,0.0,0.0,0.0},
	visual          = "sprite",
	textures        = { "invisible.png^[makealpha:128,0,0^[makealpha:128,128,0" },
	on_activate = function(self,staticdata)

		local pos = self.object:getpos();
		minetest.add_entity(pos,"mobf:compat_spawner")
		self.object:remove()
	end,
})

minetest.register_entity("animal_giant:giant_spawner_shadows",
 {
	physical        = false,
	collisionbox    = { 0.0,0.0,0.0,0.0,0.0,0.0},
	visual          = "sprite",
	textures        = { "invisible.png^[makealpha:128,0,0^[makealpha:128,128,0" },
	on_activate = function(self,staticdata)

		local pos = self.object:getpos();
		minetest.add_entity(pos,"mobf:compat_spawner")
		self.object:remove()
	end,
})

--spawning code
local giant_name   = giant_prototype.modname .. ":"  .. giant_prototype.name
local giant_env = mobf_environment_by_name(giant_prototype.generic.envid)

mobf_spawner_register("giant_spawner_1",giant_name,
	{
	spawnee = giant_name,
	spawn_interval = 2,
	spawn_inside = giant_env.media,
	entities_around =
		{
			{ type="MAX",distance=1,threshold=0 },
			{ type="MAX",entityname=giant_name,
				distance=giant_prototype.generic.population_density,threshold=2 },
		},

	absolute_height =
	{
		min = -10,
	},

	light_around =
	{
		{ type="TIMED_MIN", distance = 0, threshold=LIGHT_MAX +1,time=0.5 },
		{ type="TIMED_MAX", distance = 0, threshold=6,time=0.0 },
		{ type="CURRENT_MAX", distance = 0, threshold=5 }
	},


	surfaces = { 
		"default:dirt_with_grass", 
		"default:sand", 
		"default:desert_sand", 
		"default:snow",
		"default:dirt_with_snow",
		"default:snow_block",
		"default:dirt_with_dry_grass",
	},
	collisionbox = selectionbox_giant
	})

mobf_spawner_register("giant_spawner_2",giant_name,
	{
	spawnee = giant_name,
	spawn_interval = 60,
	spawn_inside = giant_env.media,
	entities_around =
		{
			{ type="MAX",distance=1,threshold=0 },
			{ type="MAX",entityname=giant_name,
				distance=50,threshold=2 },
		},

	light_around =
	{
		{ type="OVERALL_MAX", distance = 2, threshold=6 }
	},

	absolute_height = {
		max = 100,
	},

	mapgen =
	{
		enabled = true,
		retries = 10,
		spawntotal = 3,
	},

	collisionbox = selectionbox_giant
	})
]]

--register with animals mod
minetest.log("action","\tadding mob "..giant_prototype.name)
mobf_add_mob(giant_prototype)
minetest.log("action","MOD: animal_giant mod          version " .. version .. " loaded")