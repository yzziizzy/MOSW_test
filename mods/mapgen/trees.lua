


minetest.register_decoration({
    deco_type = "simple",
    place_on = "default:dirt_with_grass",
    fill_ratio = .2,
    sidelen = 20,
    biomes = {"tropical"},
    height = 1,
    decoration = "moretrees:jungletree_sapling_ongen"
})
minetest.register_decoration({
    deco_type = "simple",
    place_on = "default:sand",
    fill_ratio = .01,
    sidelen = 20,
    biomes = {"sandy_beach",},
    height = 3,
    decoration = "default:papyrus"
})

minetest.register_decoration({
    deco_type = "simple",
    place_on = "default:dirt_with_grass",
    fill_ratio = .07,
    sidelen = 20,
    biomes = {"temperate_low","temperate_mid"},
    height = 1,
    decoration = {"flowers:tulip", "flowers:rose", "flowers:dandelion_yellow", "flowers:dandelion_white","flowers:geranium","flowers:viola"}

})


minetest.register_decoration({
    deco_type = "simple",
    place_on = "default:dirt_with_grass",
    fill_ratio = .005,
    -- noise_params = {offset=0, scale=.05, spread={x=80, y=80, z=80}, seed=354, octaves=3, persist=0.7},
    sidelen = 20,
    biomes = {"savannah"},
    height = 1,
    decoration = {"moretrees:pine_sapling_ongen", "default:grass_1", "default:jungle_grass"}
})


minetest.register_decoration({
    deco_type = "simple",
    place_on = "default:dirt_with_grass",
    fill_ratio = .001,
    sidelen = 20,
    biomes = {"temperate_low"},
    height = 1,
    decoration = {"moretrees:oak_sapling_ongen", "moretrees:beech_sapling_ongen"}
})
minetest.register_decoration({
    deco_type = "simple",
    place_on = "default:dirt_with_grass",
    fill_ratio = .003,
    sidelen = 20,
    biomes = {"temperate_mid"},
    height = 1,
    decoration = "moretrees:birch_sapling_ongen"
})

minetest.register_decoration({
    deco_type = "simple",
    place_on = "default:dirt_with_grass",
    fill_ratio = .003,
    sidelen = 20,
    biomes = {"pacnw"},
    height = 1,
    decoration = "moretrees:fir_sapling_ongen"
})

minetest.register_decoration({
    deco_type = "simple",
    place_on = "default:dirt_with_snow",
    fill_ratio = .001,
    sidelen = 20,
    biomes = {"tiaga"},
    height = 1,
    decoration = "moretrees:spruce_sapling_ongen"
})
minetest.register_decoration({
    deco_type = "simple",
    place_on = "default:sand",
    fill_ratio = .001,
    sidelen = 20,
    biomes = {"sandy_beach"},
    height = 1,
    decoration = "moretrees:palm_sapling_ongen"
})


minetest.register_decoration({
    deco_type = "simple",
    place_on = "default:desert_sand",
    fill_ratio = .9,
    sidelen = 80,
    biomes = {"scrub"},
    height = 1,
    decoration = {"default:dry_shrub","default:grass_1"}
})
