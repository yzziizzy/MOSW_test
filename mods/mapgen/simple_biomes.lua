
local function add_biome(name, heat, humid, ymin, ymax, fill_cnt, fill_name, top_cnt, top_name, dust)

    minetest.register_biome({
        name = name,

        node_top       = top_name,
        depth_top      = top_cnt,
        node_filler    = fill_name,
        depth_filler   = fill_cnt,
        node_dust      = dust,
--      node_dust_water
		
        y_min     = ymin,
        y_max     = ymax,
        heat_point     = heat,
        humidity_point = humid,
    })


end

--[[
glacier
tundra
tiaga
temperate
subtropical
tropical
beaches
desert
scrubs
alpine
barren alpine

]]
--                        humidity ymax   fillname          topname
--         name        heat     ymin  fillcnt         topcnt                  dust

--beaches
add_biome("sandy_beach", 50,100,  1,4, 1,"default:sand", 1,"default:sand", "air")
add_biome("gravel_beach", 0, 50,  1,4, 1,"default:mossycobble", 1,"default:gravel", "air")
add_biome("desert_beach", 100,0,  1,4, 1,"default:desert_sand", 1,"default:sand", "air")
--[ [
-- ground
add_biome("tiaga",     0, 80, 5,180, 2,"default:dirt", 1,"default:dirt_with_snow", "air")
add_biome("pacnw",    33, 80, 5,180, 2,"default:dirt", 1,"default:dirt_with_grass", "air")
add_biome("temperate_low",66, 80,  5,50, 2,"default:dirt", 1,"default:dirt_with_grass", "air")
add_biome("temperate_mid",66, 80,  50,180, 2,"default:dirt", 1,"default:dirt_with_grass", "air")
add_biome("tropical",100, 80, 5,180, 2,"default:dirt",       1,"default:dirt_with_grass", "air")

add_biome("tundra",   10, 0, 5,180, 1,"default:ice", 3,"default:snowblock", "air")
add_biome("scrub",    40, 0,   5,180, 1,"default:dirt", 1,"default:desert_sand", "air")
add_biome("savannah", 55, 0,  5,180, 3,"default:dirt",       1,"default:dirt_with_grass", "air")
add_biome("desert",  100, 0,   5,180, 7,"default:desert_sand", 1,"default:desert_sand", "air")

-- mountains
add_biome("active volcano", -10,51,240,293,1,"default:lava_source", 20,"air", "air")
add_biome("dormant volcano", -10,49,235,240,1,"default:obsidian", 8,"default:obsidian", "air")

add_biome("cold mtn",  0,50,180,31000,1,"default:cobble", 1,"default:gravel", "default:snow")
add_biome("temp mtn", 50,50,180,31000,1,"default:cobble", 2,"default:gravel", "air")
add_biome("hot mtn", 100,50,180,31000,0,"default:cobble", 1,"default:gravel", "air")

-- ocean
add_biome("icebergs", 10,50,-20,0,1,"default:dirt", 1,"default:sand","default:ice")

add_biome("sandy ocean floor", 100,50,-31000,0,1,"default:sand",1,"default:sand", null)
add_biome("gravelly ocean floor", 50,50,-31000,0,1,"default:gravel", 1,"default:gravel", null)
add_biome("rock ocean floor", 0,50,-31000,0,1, "default:stone",  1, "default:stone",  null)


-- ]]
-- old garbage
--[[
add_biome("glacier", 0,80,5,120,9,"default:ice", 1,"default:ice", "air")
add_biome("glacier edge", 5,78,5,120,3,"default:dirt", 3,"default:ice", "default:ice")
add_biome("very wet tundra", 10,100,5,180,3,"default:ice", 8,"default:snowblock", "default:snowblock")
add_biome("wet tundra", 10,75,5,180,5,"default:dirt", 4,"default:snowblock", "default:snowblock")
add_biome("normal tundra", 10,50,5,180,7,"default:dirt", 2,"default:snowblock", "default:snowblock")
add_biome("dry tundra", 10,25,5,180,8,"default:dirt", 1,"default:snowblock", "default:snowblock")
add_biome("very dry tundra", 10,0,5,180,3,"default:dirt", 8,"default:dirt", "default:snowblock")

add_biome("wet permafrost", 15,75,5,180,6, "default:dirt",  2, "default:dirt",  "default:snowblock")
add_biome("dry permafrost", 15,25,5,180,6, "default:dirt",  2, "default:dirt",  "air")
add_biome("wet barrens", 25,50,5,180,6, "default:dirt",  2,"default:dirt", "default:snowblock")
add_biome("dry barrens", 25,10,5,180,6, "default:dirt",  2,"default:dirt", "air")
add_biome("boreal fir forest", 35,75,5,180,2, "default:dirt",  6,"default:dirt", "air")
add_biome("boreal spruce forest", 35,25,5,180,2, "default:dirt",  6,"default:dirt", "air")
add_biome("boreal mixed forest", 35,50,5,180,2, "default:dirt",  6,"default:dirt", "air")

add_biome("sand dunes", 50,0,5,150,3,"default:stone", 8,"default:sand","default:sand")
add_biome("grassy dunes", 50,25,5,150,3,"default:stone", 8,"default:sand","default:sand")
add_biome("tempplain", 50,50,5,150,3,"default:stonecobble", 5,"default:dirt_with_grass", "default:snow")
add_biome("badlands", 60,25,5,150,4,"default:desertstone", 4,"default:gravel", "default:clay")


add_biome("icy beach", -1,50,1,7,1,"default:stonecobble", 1,"default:ice", "default:ice")
add_biome("wet icy beach", -1,100,1,7,1,"default:ice", 1,"default:ice", "default:ice")
add_biome("cold sandy beach", 0,100,1,10,1,"default:sand",1,"default:sand","default:snowblock")
add_biome("cold gravelly beach", 0,50,1,7,1,"default:stonecobble", 1,"default:gravel", "default:snowblock")
add_biome("cold rocky beach", 0,0,1,5,0,0,"default:snowblock")
add_biome("temp sandy beach", 50,100,1,10,1,"default:sand",1,"default:sand","air")
add_biome("temp gravelly beach", 50,50,1,7,1,"default:gravel", 1,"default:gravel", "air")
add_biome("temp rocky beach", 50,0,1,5,0,0,"air")
add_biome("warm sandy beach", 100,100,1,10,1,"default:sand",1,"default:sand","default:grass")
add_biome("warm gravel beach", 100,50,1,7,1,"default:mossycobble", 1,"default:gravel", "default:grass")
add_biome("warm rocky beach", 100,0,1,5,0,0,"default:grass")

add_biome("icebergs", 10,50,-20,0,1,"default:dirt", 1,"default:sand","default:ice")

add_biome("sandy ocean floor", 50,50,-31000,0,1,"default:sand",1,"default:sand","default:sand")
add_biome("gravelly ocean floor", 50,50,-31000,0,1,"default:gravel", 1,"default:gravel", "default:gravel")
add_biome("rock ocean floor", 50,50,-31000,0,1, "default:stone",  1, "default:stone",  "default:stonecobble")


add_biome("active volcano", -10,51,240,243,1,"default:lava_source", 20,"air", "air")
add_biome("dormant volcano", -10,49,235,31000,1,"default:obsidian", 8,"default:obsidian", "air")
add_biome("rocky mountain", 70,10,180,31000,1, "default:stone",  1,"default:stonecobble", "air")
add_biome("snowy mountain", 30,70,180,31000,1,"default:stonecobble", 1,"default:gravel", "default:snowblock")
add_biome("snowy-gravelly mtn", 30,50,180,31000,1,"default:stonecobble", 2,"default:gravel", "default:snowblock")
add_biome("gravelly mtn", 40,30,180,31000,1,"default:stonecobble", 2,"default:gravel", "air")
add_biome("stone mtn", 60,60,180,31000,2, "default:stone",  1, "default:stone",  "air")
add_biome("snowy stone mtn", 10,100,180,31000,2, "default:stone",  1, "default:stone",  "default:snowblock")



--]]





--[[

minetest.register_biome({
    name = "volcano",

    node_top       = "air",
    depth_top      = 10,
    node_filler    = "default:lava_source",
    depth_filler   = 1,
    node_dust      = "air",

    height_min     = 240,
    height_max     = 2000,
    heat_point     = 50,
    humidity_point = 40,
})


minetest.register_biome({
    name = "plains",

    node_top       = "default:dirt",
    depth_top      = 1,
    node_filler    = "default:dirt",
    depth_filler   = 3,
    node_dust      = "default:dirt_with_grass",

    height_min     = -300,
    height_max     = 120,
    heat_point     = 100,
    humidity_point = 40,
})



minetest.register_biome({
    name = "tundra_wet_low",

    node_top       = "default:snowblock",
    depth_top      = 8,
    node_filler    = "default:dirt",
    depth_filler   = 4,
    node_dust      = "",

    height_min     = 1,
    height_max     = 70,
    heat_point     = 5,
    humidity_point = 70,
})

minetest.register_biome({
    name = "tundra_wdry_low",

    node_top       = "default:snowblock",
    depth_top      = 1,
    node_filler    = "default:dirt",
    depth_filler   = 6,
    node_dust      = "",

    height_min     = 1,
    height_max     = 70,
    heat_point     = 5,
    humidity_point = 10,
})

minetest.register_biome({
    name = "tundra_dry_high",

    node_top       = "default:snowblock",
    depth_top      = 1,
    node_filler    = "default:dirt",
    depth_filler   = 3,
    node_dust      = "",

    height_min     = 60,
    height_max     = 120,
    heat_point     = 5,
    humidity_point = 10,
})

minetest.register_biome({
    name = "tundra_wet_high",

    node_top       = "default:snowblock",
    depth_top      = 8,
    node_filler    = "default:dirt",
    depth_filler   = 2,
    node_dust      = "",

    height_min     = 60,
    height_max     = 120,
    heat_point     = 5,
    humidity_point = 10,
})

minetest.register_biome({
    name = "glacier",

    node_top       = "default:ice",
    depth_top      = 7,
    node_filler    = "default:ice",
    depth_filler   = 4,
    node_dust      = "",

    height_min     = 1,
    height_max     = 120,
    heat_point     = 5,
    humidity_point = 80,
})
minetest.register_biome({
    name = "glacier_edge",

    node_top       = "default:ice",
    depth_top      = 3,
    node_filler    = "default:dirt",
    depth_filler   = 2,
    node_dust      = "default:dry_shrub",

    height_min     = 1,
    height_max     = 120,
    heat_point     = 10,
    humidity_point = 70,
})
]]
-- minetest.register_biome({
--     name = "tiaga",
--
--     node_top       = "default:dirt",
--     depth_top      = 1,
--     node_filler    = "default:dirt",
--     depth_filler   = 3,
--     node_dust      = "default:dirt_with_grass",
--
--     height_min     = -300,
--     height_max     = 2000,
--     heat_point     = 50,
--     humidity_point = 40,
-- })
--









minetest.register_biome({
    name = "mountain_top",

    node_top       = "default:gravel",
    depth_top      = 1,
    node_filler    = "default:dirt",
    depth_filler   = 0,
    node_dust      = "default:snow",

    height_min     = 220,
    height_max     = 2000,
    heat_point     = 50,
    humidity_point = 50,
})
























