 
minetest.register_node("torches:flame", {
    description = "Torch Flame",
    drawtype = "plantlike",
    tiles = {{
        name="fire_basic_flame_animated.png",
        animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=1},
    }},
    inventory_image = "fire_basic_flame.png",
    light_source = 14,
    groups = {igniter=1,hot=1},
    drop = '',
    waving=1,
    walkable = false,
    buildable_to = true,
    damage_per_second = 1,
    
})
minetest.register_node("torches:torchbase", {
    description = "Fancy Torch",
    tiles = {"default_desert_stone.png"},
    groups = {cracky=3},
    sounds = default.node_sound_stone_defaults(),
    drawtype = "nodebox",
    paramtype = "light",
    node_box = {
        type = "fixed",
        fixed = {
            -- stem
           {-0.25, -0.5, -0.25, 0.25, 0.10, 0.25},
           
           --bowl
           {-0.38, -0.2, -0.38, 0.38, 0.10, 0.38},
          --  {-0.2, -0.2, -0.2, 0.2, 0.2, 0.2},
           {-0.5, 0.10, -.5, 0.5, 0.5, 0.5},
           --base 
           {-0.5, -0.5, -0.5, 0.5, -0.4, 0.5},
        },
    },
    
    on_punch = function(pos, node, puncher)
        if puncher:get_wielded_item():get_name() == "default:torch" then
            -- check if unlit and light
            pos.y = pos.y + 1
            local node =  minetest.get_node(pos)
            if node.name == "air" then
                minetest.set_node(pos, {name="torches:flame"})
            end
        end
    end,
    on_destruct = function(pos)
        pos.y = pos.y + 1
        local node =  minetest.get_node(pos)
        if node.name == "torches:flame" then
            minetest.set_node(pos, {name="air"})
        end

    end,
    
})


minetest.register_craft({
    output = "torches:torchbase",
    recipe = {
        {"", "", ""},
        {"group:stone", "default:coal_lump", "group:stone"},
        {"", "group:stone", ""}
    }
})

