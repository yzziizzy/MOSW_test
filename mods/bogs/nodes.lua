 
minetest.register_node("bogs:living_peat", {
    description = "Sphagnum Moss",
    drawtype = "nodebox",
    paramtype = "light",  
    tiles = {"wool_green.png"},
    leveled = 4,
    is_ground_content = true,
    groups = {crumbly=3,soil=1,falling_node=1},
    node_box = {
        type = "leveled",
        fixed = {
            {-0.5, -0.5, -0.5, 0.5, -0.375+2/16, 0.5}, -- NodeBox1
            {-0.0625, -0.5, -0.0625, 0.375, -0.3125+2/16, 0.375}, -- NodeBox2
            {-0.4375, -0.5, -0.375, 0.125, -0.3125+2/16, 0.0625}, -- NodeBox3
        }
    }
})


minetest.register_node("bogs:wet_peat", {
    description = "Wet Peat",
    tiles = { "wool_brown.png" },
    is_ground_content = true,
    groups = {crumbly=3,soil=1},
    sounds = default.node_sound_stone_defaults(),
}) 
minetest.register_node("bogs:dry_peat", {
    description = "Dry Peat",
    tiles = { "wool_black.png" },
    is_ground_content = true,
    groups = {crumbly=3,soil=1, flammable=3},
    sounds = default.node_sound_stone_defaults(),
}) 

-- wet peat should dry out if not near water
minetest.register_abm({
    nodenames = {"bogs:wet_peat"},
    interval = 2,
    chance = 2,
    action = function(pos, node)
        if nil == minetest.find_node_near(pos, 3, "group:water") then
            minetest.set_node(pos, {name = "bogs:dry_peat"})
        end
    end
})

-- dry peat soaks up water
minetest.register_abm({
    nodenames = {"bogs:dry_peat"},
    neighbors = {"bogs:wet_peat", "bogs:living_peat", "group:water"},
    interval = 2,
    chance = 2,
    action = function(pos, node)
        if nil ~= minetest.find_node_near(pos, 3, "group:water") then
            minetest.set_node(pos, {name = "bogs:wet_peat"})
        end
    end
})

-- dry peat soaks up water
minetest.register_abm({
    nodenames = {"bogs:living_peat"},
    interval = 1,
    chance = 1,
    action = function(pos, node)
        sub = minetest.find_node_near(pos, 2, "group:soil")
        -- something is very very wrong here...
        if sub == nil then return end
        -- if sub.y > pos.y then return end
        sub.y = sub.y + 1
        local target =  minetest.get_node(sub)
        if "air" == target.name and nil ~= minetest.find_node_near(sub, 3, "group:water") then
            minetest.set_node(sub, {name = "bogs:living_peat"})
        end
    end
})



minetest.register_craftitem("bogs:compressed_peat", {
    description = "Compressed Peat Brick",
    inventory_image = "geology_chalk_powder.png",
})

minetest.register_craft({
    output = 'bogs:compressed_peat',
    recipe = {
        {'bogs:dry_peat', 'bogs:dry_peat'},
        {'bogs:dry_peat', 'bogs:dry_peat'},
    }
})

minetest.register_craft({
    type = "fuel",
    recipe = "bogs:living_peat",
    burntime = 1, -- it's wet and green, what do you expect?
})

minetest.register_craft({
    type = "fuel",
    recipe = "bogs:wet_peat",
    burntime = 10, -- it's wet, what do you expect?
})

minetest.register_craft({
    type = "fuel",
    recipe = "bogs:dry_peat",
    burntime = 20, -- it's so-so
})

minetest.register_craft({
    type = "fuel",
    recipe = "bogs:compressed_peat",
    burntime = 60, 
})